import { Controller, Get, Query, Req } from '@nestjs/common';
import { Request } from 'express'
import { ExchangeService } from './../services/exchange/exchange.service';

@Controller('exchange')
export class ExchangeController {
  constructor(private exchangeService: ExchangeService) {}

  @Get()
  get(
    @Query('from') from: string,
    @Query('to') to: string,
    @Query('amount') amount: number,
  ): void {
   
  }
}
export type ExternalExchangeConfig = {
  url: string;
  apiKey: string;
  isActive: boolean;
}

export default {
  'externalExchangesConfigs': {
    'currencyConverter': <ExternalExchangeConfig>{
      'url': process.env.CURRENCY_CONVERTER_API_URL,
      'apiKey': process.env.CURRENCY_CONVERTER_API_KEY,
      'isActive': process.env.CURRENCY_CONVERTER_IS_ACTIVE === 'true'
    },
  }
}
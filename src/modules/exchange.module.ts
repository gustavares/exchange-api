import { Module } from '@nestjs/common';
import { ExchangeController } from './../controllers/exchange.controller';
import { ExchangeService } from './../services/exchange/exchange.service';

@Module({
  imports: [],
  controllers: [ExchangeController],
  providers: [ExchangeService],
})
export class ExchangeModule {}

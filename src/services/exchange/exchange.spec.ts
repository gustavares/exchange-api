import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeRequest, ExchangeResponse } from './exchange.interface';
import { ExchangeService } from './exchange.service';

describe('ExchangeService', () => {
  let exchange: TestingModule;

  beforeAll(async () => {
    exchange = await Test.createTestingModule({
      providers: [ExchangeService],
    }).compile();
  });

  describe('get', () => {
    it('should return an ExchangeResponse object with 5.5 as the "value" property', () => {
      const exchangeService = exchange.get<ExchangeService>(ExchangeService)
      const req = <ExchangeRequest>{
        from: 'USD',
        to: 'BRL'
      }

      expect(exchangeService.get(req)).toEqual(<ExchangeResponse>{
        from: 'USD',
        to: 'BRL',
        value: 5.5
      })
    });
  });

});
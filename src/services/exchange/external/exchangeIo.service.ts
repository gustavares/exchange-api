import { Injectable } from '@nestjs/common';
import { ExternalExchangeConfig } from 'src/config/environment';
import { ExchangeRequest, ExchangeResponse, IExchange } from '../exchange.interface';

@Injectable()
export class ExchangeIoService implements IExchange {
  config: ExternalExchangeConfig;

  constructor() {}
  
  convert(req: ExchangeRequest): ExchangeResponse {
    throw new Error('Method not implemented.');
  }

}
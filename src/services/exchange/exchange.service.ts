import { Injectable } from '@nestjs/common';
import { IExchange, ExchangeRequest, ExchangeResponse } from './exchange.interface';

@Injectable()
export class ExchangeService {
  constructor(private exchange: IExchange) { }
  
  get(req: ExchangeRequest): ExchangeResponse {
    /**
     * check if there is the same from-to request in queue
     * 
     * if there is 
     *  wait and listen for response event
     * 
     * if not
     *  call all active external services, use response from the first to answer
     *  store in cache
     * 
     * calculate exchange
     * 
     */

    const res = this.exchange.convert(req)
    
    throw new Error('Method not implemented.');
  }

}
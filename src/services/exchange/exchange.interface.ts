import { Injectable } from "@nestjs/common"
import { ExternalExchangeConfig } from "src/config/environment"

export type ExchangeRequest = {
  from: string;
  to: string;
}

export type ExchangeResponse = {
  from: string;
  to: string;
  value: number;
}

export interface IExchange {
  config: ExternalExchangeConfig;
  
  convert(req: ExchangeRequest): ExchangeResponse
}